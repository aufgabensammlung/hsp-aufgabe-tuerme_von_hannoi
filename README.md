# Türme von Hanoi
Aufgabe Türme von Hanoi für Hochsprachenprogrammierung / Mecke an der Jade Hochschule
Ziel der Aufgabe: drei unterschiedliche große "Scheiben" in richtiger reihenfolge von einem "Turm" zu dem nächsten zu bringen.

# Folgende Schritte sind Notwendig
Alle "Scheiben" befinden sich in der Ausgangsbedingung auf dem linken "Turm" und gespielt wird in unserem fall mit 3 "Scheiben"
1. Die erste "Scheibe" wird auf den ganz rechten "Turm" gelegt 
2. die zweite "Scheibe" wird auf den mittleren "Turm" gelegt 
3. die erste "Scheibe" wird vom rechten "Turm" auf den mittleren "Turm" gelegt
4. die dritte "Scheibe" wird auf den ganz rechten "Turm" gelegt
5. die erste "Scheibe"wird von dem mittleren "Turm" auf den linken "Turm" gelegt
6. die zweite "Scheibe" wird auf den rechten "Turm" gelegt
7. die erste "Scheibe" wird auf rechten "Turm" gelegt



Eine Grafische erklärung finden Sie [hier](https://www.mathematik.ch/spiele/hanoi_mit_grafik/). 

## Getting Started
* Clonen Sie die Repository auf Ihren Rechner
* Öffnen Sie die Solution *.sln in Visual Studio
* Führen Sie die Implementierung in der Methode durch

Wenn Sie fertig sind, dann
* Im Projektmappen Explorer (engl. Solution Explorer) klicken Sie rechts das -Unittesting Projekt und wählen Sie "Tests ausführen"
* Starten Sie die Tests innerhalb des Test-Explorers

Diesen Ablauf können Sie auch mit Hilfe des Videos "Aufgabensammlung Einstieg" nachvollziehen
