using Microsoft.VisualStudio.TestTools.UnitTesting;
using Türme_von_Hanooi;
using System;
using static Türme_von_Hanooi.Program;


namespace UnitTest_Türme_von_Hanooi
{
    [TestClass]
    public class UnitTest1
    {


        [TestMethod]
        public void TestMethod1()
        {
            //Arrange
            Program prg = new Program();

            //Act



            //Assert
            var expected = "Schiebe 3 von L nach M" ;

            

            var actual = prg.Verschieben( 3, 'L', 'R', 'M');

            Assert.AreEqual(expected, actual);


        }


      

    }
    
}

